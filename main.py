import pygame
import random
import time
from pygame.locals import *

from config import *
pygame.init()
# pygame.font.init()
font = pygame.font.Font('freesansbold.ttf', 20)
score_font = pygame.font.Font('freesansbold.ttf', 40)

screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Naa game Naa ishtam")

score = 0
flag = 1
round = 1
k = 1
score2 = 0
speed = 0
scorer1 = 0
scorer2 = 0
value = 0
value2 = 0


def final1(points1, ray):
    score_val = score_font.render("Score of player1 :  " + str(points1), True, (255, 0, 0))
    screen.blit(score_val, (300, 100))
    score_vally = score_font.render("Time of player1 : " + str(ray // 170), True, (255, 0, 0))
    screen.blit(score_vally, (300, 200))


def final2(points2, ray):
    score_val = score_font.render("Score of player2 :  " + str(points2), True, (255, 0, 0))
    screen.blit(score_val, (300, 300))
    score_vally2 = score_font.render("Time of player2 : " + str(ray // 170), True, (255, 0, 0))
    screen.blit(score_vally2, (300, 400))


def player(playerx, playery):
    screen.blit(playerimage, (playerx, playery))


def movobs(movobsx, movobsy):
    screen.blit(movobsimage, (movobsx, movobsy))


def fixobs(fixobsx, fixobsy):
    screen.blit(fixobsimage, (fixobsx, fixobsy))


def cal(var, var1):
    if var >= 514 - 128:
        var1 = 0
    if var < 514 - 128 and var >= 454 - 128:
        var1 = 10
    if 414 - 128 > var >= 404 - 128:
        var1 = 20
    if var < 404 - 128 and var >= 314 - 128:
        var1 = 30
    if var < 314 - 128 and var >= 254 - 128:
        var1 = 40
    if var < 254 - 128 and var >= 194 - 128:
        var1 = 50
    if var < 194 - 128 and var > 0:
        var1 = 60
    if var == 0:
        var1 = 70
    return var1


def cal1(var, var1):
    if var <= 114:
        var1 = 0
    if var > 114 and var <= 194:
        var1 = 10
    if var > 194 and var <= 254:
        var1 = 20
    if var > 254 and var <= 314:
        var1 = 30
    if var > 314 and var <= 404:
        var1 = 40
    if var > 404 and var <= 454:
        var = 50
    if var > 454 and var <= 514:
        var1 = 60
    if var > 514:
        var1 = 70
    return var1


def printit(samay):
    score_val3 = font.render("Time :" + str(samay // 170), True, (255, 0, 0))
    screen.blit(score_val3, (0, 0))


def view1(score, round):
    score_val = font.render("Score of player1 in round :" + str(round) + " : " + str(score), True, (0, 0, 0))
    screen.blit(score_val, (30, 30))


def view2(score, round):
    score_val = font.render("Score of player2 in round :" + str(round) + " : " + str(score), True, (0, 0, 0))
    screen.blit(score_val, (470, 30))


def viewit1(score):
    over = score_font.render("GAME OVER", True, (255, 0, 0))
    score_val = score_font.render("Score of player1 is :" + str(score), True, (0, 0, 0))
    screen.blit(score_val, (300, 200))
    screen.blit(over, (300, 300))
    score_1 = score_font.render("Click SPACE to continue", True, (255, 0, 0))
    screen.blit(score_1, (200, 400))


def viewit2(score):
    over = score_font.render("GAME OVER", True, (255, 0, 0))
    score_val = score_font.render("Score of player2 is :" + str(score), True, (0, 0, 0))
    screen.blit(score_val, (300, 200))
    screen.blit(over, (300, 300))
    score_1 = score_font.render("Click SPACE to continue", True, (255, 0, 0))
    screen.blit(score_1, (200, 400))


set1 = 0
set2 = 0
playerx1 = 370
playery1 = 5

playerimage = pygame.image.load('male.png')
movobsimage = pygame.image.load('boat.png')
fixobsimage = pygame.image.load('mountain.png')
running = True
playerx = 370
playery = 540
playerx_change = 0
playery_change = 0
playerx_change1 = 0
playery_change1 = 0
movobsx_change = 0.3
movobsx_change1 = 0.3
movobsx_change2 = 0.3
movobsx_change3 = 0.3
movobsx_change4 = 0.3
movobsx_change5 = 0.3
movobsx = random.randint(0, 736)
movobsy = 50
movobsx1 = random.randint(0, 736)
movobsy1 = 190
movobsx2 = random.randint(0, 736)
movobsy2 = 340
movobsx3 = random.randint(0, 736)
movobsy3 = 450
fixobsx = random.randint(0, 736)
fixobsy = 130
fixobsx1 = random.randint(0, 736)
fixobsy1 = 130
fixobsx2 = random.randint(0, 736)
fixobsy2 = 250
fixobsx3 = random.randint(0, 736)
fixobsy3 = 250
fixobsx4 = random.randint(0, 736)
fixobsy4 = 390
fixobsx5 = random.randint(0, 736)
fixobsy5 = 390
t = 1
t2 = 0
while running:
    if round <= 3:
        if round == 1:
            speed = 0.3
        if round == 2:
            speed = 0.6
        if round == 3:
            speed = 0.9
        if flag == 1 and k % 2 == 1:
            t += 1
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
            screen.fill((135, 206, 235))
            pygame.draw.rect(screen, (0, 100, 0), (0, 0, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 130, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 250, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 390, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 536, 800, 64))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        playerx_change = -0.3
                    if event.key == pygame.K_RIGHT:
                        playerx_change = 0.3
                    if event.key == pygame.K_UP:
                        playery_change = -0.3
                    if event.key == pygame.K_DOWN:
                        playery_change = 0.3
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        playerx_change = 0
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                        playery_change = 0
            playerx += playerx_change
            if playerx <= 0:
                playerx = 0
            elif playerx >= 736:
                playerx = 736
            playery += playery_change
            if playery <= 0:
                playery = 0
            elif playery >= 536:
                playery = 536
            movobsx += movobsx_change
            if movobsx <= 0:
                movobsx_change = speed
            elif movobsx >= 736:
                movobsx_change = -speed
            movobsx1 += movobsx_change1
            if movobsx1 <= 0:
                movobsx_change1 = speed
            elif movobsx1 >= 736:
                movobsx_change1 = -speed
            movobsx2 += movobsx_change2
            if movobsx2 <= 0:
                movobsx_change2 = speed
            elif movobsx2 >= 736:
                movobsx_change2 = -speed
            movobsx3 += movobsx_change3
            if movobsx3 <= 0:
                movobsx_change3 = speed
            elif movobsx3 >= 736:
                movobsx_change3 = -speed
            player(playerx, playery)
            movobs(movobsx, movobsy)
            movobs(movobsx1, movobsy1)
            movobs(movobsx2, movobsy2)
            movobs(movobsx3, movobsy3)
            fixobs(fixobsx, fixobsy)
            fixobs(fixobsx1, fixobsy1)
            fixobs(fixobsx2, fixobsy2)
            fixobs(fixobsx3, fixobsy3)
            fixobs(fixobsx4, fixobsy4)
            fixobs(fixobsx5, fixobsy5)
            score1 = cal(playery, score)
            if abs(playerx - movobsx) <= 64 and abs(playery - movobsy) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - movobsx1) <= 64 and abs(playery - movobsy1) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - movobsx2) <= 64 and abs(playery - movobsy2) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - movobsx3) <= 64 and abs(playery - movobsy3) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx) <= 64 and abs(playery - fixobsy) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx1) <= 64 and abs(playery - fixobsy1) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx2) <= 64 and abs(playery - fixobsy2) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx3) <= 64 and abs(playery - fixobsy3) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx4) <= 64 and abs(playery - fixobsy4) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if abs(playerx - fixobsx5) <= 64 and abs(playery - fixobsy5) <= 64:
                playerx = 370
                playery = 540
                flag = 0
                pygame.display.update()
            if score1 == 70:
                x = 370
                y = 540
                flag = 0
                set2 = 1
            view1(score1, round)
            view2(score2, round)
            printit(t)
        if flag == 0:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            screen.fill((0, 119, 0))
            if k % 2 == 1:
                if set2 == 1:
                    scorer1 += score1
                    set2 = 0
                    score1 = 0
                viewit1(scorer1)
            if k % 2 == 0:
                if set1 == 1:
                    scorer2 += score2
                    set1 = 0
                    score2 = 0
                viewit2(scorer2)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if flag == 0 and k % 2 == 0:
                        round += 1
                    flag = 1
                    k += 1
        if flag == 1 and k % 2 == 0:
            t2 += 1
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
            screen.fill((135, 206, 235))
            pygame.draw.rect(screen, (0, 100, 0), (0, 0, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 130, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 250, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 390, 800, 64))
            pygame.draw.rect(screen, (0, 100, 0), (0, 536, 800, 64))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        playerx_change1 = -0.3
                    if event.key == pygame.K_RIGHT:
                        playerx_change1 = 0.3
                    if event.key == pygame.K_UP:
                        playery_change1 = -0.3
                    if event.key == pygame.K_DOWN:
                        playery_change1 = 0.3
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        playerx_change1 = 0
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                        playery_change1 = 0
            playerx1 += playerx_change1
            if playerx1 <= 0:
                playerx1 = 0
            elif playerx1 >= 736:
                playerx1 = 736
            playery += playery_change
            if playery1 <= 0:
                playery1 = 0
            elif playery1 >= 536:
                playery1 = 536
            movobsx += movobsx_change
            if movobsx <= 0:
                movobsx_change = speed
            elif movobsx >= 736:
                movobsx_change = -speed
            movobsx1 += movobsx_change1
            if movobsx1 <= 0:
                movobsx_change1 = speed
            elif movobsx1 >= 736:
                movobsx_change1 = -speed
            movobsx2 += movobsx_change2
            if movobsx2 <= 0:
                movobsx_change2 = speed
            elif movobsx2 >= 736:
                movobsx_change2 = -speed
            movobsx3 += movobsx_change3
            if movobsx3 <= 0:
                movobsx_change3 = speed
            elif movobsx3 >= 736:
                movobsx_change3 = -speed
            player(playerx1, playery1)
            movobs(movobsx, movobsy)
            movobs(movobsx1, movobsy1)
            movobs(movobsx2, movobsy2)
            movobs(movobsx3, movobsy3)
            fixobs(fixobsx, fixobsy)
            fixobs(fixobsx1, fixobsy1)
            fixobs(fixobsx2, fixobsy2)
            fixobs(fixobsx3, fixobsy3)
            fixobs(fixobsx4, fixobsy4)
            fixobs(fixobsx5, fixobsy5)
            score2 = cal1(playery1, score2)
            if abs(playerx1 - movobsx) <= 64 and abs(playery1 - movobsy) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - movobsx1) <= 64 and abs(playery1 - movobsy1) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - movobsx2) <= 64 and abs(playery1 - movobsy2) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - movobsx3) <= 64 and abs(playery1 - movobsy3) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx) <= 64 and abs(playery1 - fixobsy) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx1) <= 64 and abs(playery1 - fixobsy1) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx2) <= 64 and abs(playery1 - fixobsy2) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx3) <= 64 and abs(playery1 - fixobsy3) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx4) <= 64 and abs(playery1 - fixobsy4) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            if abs(playerx1 - fixobsx5) <= 64 and abs(playery1 - fixobsy5) <= 64:
                playerx1 = 370
                playery1 = 20
                flag = 0
                set1 = 1
            view2(score2, round)
            view1(score1, round)
            printit(t2)
    else:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        screen.fill((0, 119, 0))
        final2(scorer2, t2)
        final1(scorer1, t)
        if score1 < score2:
            score_val = score_font.render("PLAYER2 WON", True, (255, 0, 0))
            screen.blit(score_val, (300, 500))
        if score1 > score2:
            score_val = score_font.render("PLAYER1 WON", True, (255, 0, 0))
            screen.blit(score_val, (300, 500))
        if score1 == score2:
            score_val = score_font.render("DRAW", True, (255, 0, 0))
            screen.blit(score_val, (300, 500))
        score_val2 = score_font.render("click Esc to exit", True, (255, 0, 0))
        screen.blit(score_val2, (300, 600))
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            running = False

    pygame.display.update()
